<?php

// фильтр домов по выбранному интервалу проживания

if (isset($_REQUEST['select_checkin'])) { $select_checkin = $_REQUEST['select_checkin']; }  	// считываем выбранную дату заезда 
if (isset($_REQUEST['select_checkout'])) { $select_checkout = $_REQUEST['select_checkout']; }  	// считываем выбранную дату выезда

require_once '../../base/connection_base.php';

$select_checkin = preg_split('/-/', $select_checkin);
$select_checkout = preg_split('/-/', $select_checkout);


$rate_bufer =  $db->query('SELECT * FROM rate');			// обращаемся к таблице цен для вывода массива номеров
$read_rate = $rate_bufer->fetchAll();

$array_numbers = array();
$counter_numbers = 0;

foreach ($read_rate as $rate_field)
	{
		$counter_numbers++;
		if ($rate_field['block'] != '0')       // если дом не заблокирован
			{
				$array_numbers[$counter_numbers] = $rate_field['number_house'];						// выводим name_room для каждого элемента массива
			}
		else
			{
				$array_numbers[$counter_numbers] = 0;
			}
	}
	

$number_bufer = $db->query('SELECT * FROM numbers');			// обращаемся к таблице прайса номеров
$read_number = $number_bufer->fetchAll();

$array_free_room = array();        // массив свободных комнат для многокомнатных номеров
$j = 0;

for ($i=1; $i<=count($array_numbers); $i++)
	{
		$occupied = 0;                                                  // проверка дат на занятость
		foreach ($read_number as $number_field)
			{				
				if ($number_field['year']==$select_checkin[0] && $number_field['month']==$select_checkin[1] && $number_field['date']==$select_checkin[2])
					{										
						 $switch = 1;	
					}
				if ($number_field['year']==$select_checkout[0] && $number_field['month']==$select_checkout[1] && $number_field['date']==$select_checkout[2])
					{										
						 $switch = 0;	
					}
				if ($switch == 1)
					{
						if ($number_field['number_'.$i] != '')
							{
								$occupied = 1;
							}
					}
			}
		if ($occupied != 0)
			{
				echo '<script>	
						document.getElementById("house_'.$array_numbers[$i].'").style.display = "none";
						document.getElementById("house_'.$array_numbers[$i].'_block").style.display = "block"
						document.getElementById("button_house_'.$array_numbers[$i].'").style.display = "none";
					</script>';
			}
		else
			{
				if ($array_numbers[$i] != 0)
					{
						echo '<script>	
								document.getElementById("house_'.$array_numbers[$i].'").style.display = "block";
								document.getElementById("house_'.$array_numbers[$i].'_block").style.display = "none"
								document.getElementById("button_house_'.$array_numbers[$i].'").style.display = "block";
							</script>';
						$j++;
						$array_free_room[$j] = $array_numbers[$i];
					}
			}
	}
	


for ($j=1; $j<=count($array_free_room); $j++)
	{
		if ($array_free_room[$j] != 0)
			{
				echo '<script>	
						document.getElementById("house_'.$array_free_room[$j].'").style.display = "block";
						document.getElementById("house_'.$array_free_room[$j].'_block").style.display = "none"
						document.getElementById("button_house_'.$array_free_room[$j].'").style.display = "block";
					</script>';
			}
	}
	