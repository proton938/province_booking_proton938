<meta charset = "utf-8">

<?php 


date_default_timezone_set('Asia/Yekaterinburg'); // часовой пояс по Екатеринбургу

require_once '../base/connection_base.php';

$all_files = scandir('../history');

echo '<br><br>'.count($all_files).'<br><br>';

for ($i=3; $i<count($all_files); $i++)
	{
		echo $all_files[$i].' ';
		
		$file = '../history/'.$all_files[$i];														// открываем файл оперативной истории для чтения	
		$contents = file_get_contents($file); 														// считываем содержимое
		$contents = preg_split('/;/', $contents);													// разбиваем на массив по регулярному выражению ";"
		
		$date = new DateTime(date('Y-m-d H:i:s', filemtime($file)));		// определяем возраст сессии
		$year_ago = $date->diff(new DateTime)->format('%y');				// сколько лет
		$month_ago =  $date->diff(new DateTime)->format('%m');				// сколько месяцев
		$day_ago =  $date->diff(new DateTime)->format('%d');				// сколько дней
		$hour_ago = $date->diff(new DateTime)->format('%h');				// сколько часов
		$minute_ago = $date->diff(new DateTime)->format('%i');				// сколько минут
		
		echo '<br><br>'.$year_ago.' '.$month_ago.' '.$day_ago.' '.$hour_ago.' '.$minute_ago.'<br><br>';
		
		echo $contents[0].' '.$contents[1].'<br>';
		
		echo count($contents).'<br>';
		
		if ($year_ago != 0 or $month_ago != 0 or $day_ago != 0 or $hour_ago != 0)    // если сессия не обновлялась больше часа
			{
				if (count($contents) <= 2)						// если в файле сессии не выбрано ни одной даты - можем просто удалять файл
					{
						unlink($file);
					}
				else											
					{
						/* если в файле сессии есть выбранные даты - 
						необходимо произвести очистку в базе 
						и только после этого можем удалить файл */
						
						for ($j = 1; $j < count($contents); $j++)
							{
								$booking = preg_split('/,/', $contents[$j]);
								
								$bufer_numbers = $db->query('SELECT * FROM numbers WHERE id = "'.$booking[0].'"');
								$read_numbers = $bufer_numbers->fetchAll(); 
								
								foreach ($read_numbers as $number_field)
									{
										$user = $number_field['user_'.$booking[2]];
									}
									
								if ($user == '')
									{		
										$db->query('UPDATE numbers SET number_'.$booking[2].' = ""  WHERE id = "'.$booking[0].'"');
									}
							}
							
						unlink($file);
					}
			}
		
		if ($minute_ago > 2)    // если сессия не обновлялась десять минут 
			{
				if (count($contents) > 2)						//  если в файле сессии есть выбранные даты - необходимо произвести очистку в базе 
					{
						for ($j = 1; $j < count($contents); $j++)
							{
								$booking = preg_split('/,/', $contents[$j]);
								
								$bufer_numbers = $db->query('SELECT * FROM numbers WHERE id = "'.$booking[0].'"');
								$read_numbers = $bufer_numbers->fetchAll(); 
								
								foreach ($read_numbers as $number_field)
									{
										$user = $number_field['user_'.$booking[2]];
									}
									
								if ($user == '')
									{		
										$db->query('UPDATE numbers SET number_'.$booking[2].' = ""  WHERE id = "'.$booking[0].'"');
									}
							}
						$date_today = date("H:i:s_d.m.Y");
						
						$f = fopen('../history/'.$all_files[$i], 'w');
						fwrite($f, $date_today.";\n");									// Пишем текущую дату и время в файл оперативной истории
						fclose($f);
					}
			}
	}



/*

		// дата и время создания сессии
		$year_create = date('Y', filemtime($file));													
		$month_create = date('m', filemtime($file));
		$day_create = date('d', filemtime($file));
		$hour_create = date('H', filemtime($file));
		$minute_create = date('i', filemtime($file));
		
		// текущие дата и время сессии
		$year_current = date('Y');
		$month_current = date('m');
		$day_current = date('d');
		$hour_current = date('H');
		$minute_current = date('i');
		
		echo '<br><br>'.$year_create.'.'.$month_create.'.'.$day_create.' '.$hour_create.'.'.$minute_create;
		echo '<br>'.$year_current.'.'.$month_current.'.'.$day_current.' '.$hour_current.'.'.$minute_current.'<br><br>';








date_default_timezone_set('Asia/Yekaterinburg'); // часовой пояс по Екатеринбургу

$date_today = date("Y-m-d-H-i");

echo $date_today;

//Создает XML-строку и XML-документ при помощи DOM 
$dom = new DomDocument('1.0', 'UTF-8');

//добавление корня - <data> 
$data = $dom->appendChild($dom->createElement('data'));

//добавление корня - <table> 
$table = $data->appendChild($dom->createElement('table'));
// добавление элемента текстового узла <table> в <table> 
$table->appendChild($dom->createTextNode($date_today));

//генерация xml 
$dom->formatOutput = true; // установка атрибута formatOutput
						   // domDocument в значение true 
// save XML as string or file 
$test1 = $dom->saveXML(); // передача строки в файл

$dom->save('../requests/import/test_'.$date_today.'.xml'); // сохранение файла 

?>